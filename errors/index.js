import Logger from "../helpers/logger.js";

export async function logErrorHandler(err, req, res, next) {
    res.status(500).send('Un Expected Error!');
    Logger.error(`${err.status || 500} - ${res.statusMessage} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
}

export async function notFoundHandler(req, res, next) {
    res.status(404).send('The Requested Resource Not Found!');
    Logger.error(`${req.status || 404} - ${res.statusMessage} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
}

export async function apiHandler(req, res, next) {
    next()
    Logger.info(`${req.originalUrl} - ${req.method} - ${req.ip}`);
 }