import dotenv from 'dotenv';
import express from 'express';
import helmet from 'helmet';
import router from './Routes/index.js';
import { apiHandler, logErrorHandler, notFoundHandler } from './errors/index.js';
import cors from 'cors'
dotenv.config();

// Initialize Express
const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(cors())

// API Logger
app.use(apiHandler);

// Routers
app.get('/test', (req, res) => {
	console.log('test');
});
app.use('/api', router);

// Capture 500 errors
app.use(logErrorHandler);
// Capture 404 erors
app.use(notFoundHandler);

// Start the server
app.listen(process.env.port, () => {
	console.log(`listining on ${process.env.port}`);
});
