import axios from 'axios';
import GifServiceAPI from '../Service/GifService.js';

export async function index(req, res) {
	// 2- build the paging system
	const gifService = new GifServiceAPI();
	req.query.limit = req.query.limit ?? 20;
	req.query.page = req.query.page ?? 0;
	gifService.query('q', req.query.q);
	gifService.query('limit', req.query.limit);
	gifService.query('offset', req.query.page);
	gifService.query('rating', 'g');
	gifService.query('lang', 'en');
	let response = await axios({
		method: 'get',
		url: gifService.build(),
	});
	res.json({
		data: response.data.data,
		page: 0,
		numberOfPages: Math.floor(response.data.pagination.total_count / req.query.limit),
	}).status(200);
}
