import { Router } from 'express';
import * as GifController from '../Controller/GifController.js'

const router = new Router();

router.get('/gifs', GifController.index)

export default router