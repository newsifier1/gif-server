import winston from 'winston'

const levels = {
    error: 0,
    warn: 1,
    info: 2,
    http: 3,
    verbose: 4,
    debug: 5,
    silly: 6
};

const colors = {
    error: 'red',
    warn: 'yellow',
    info: 'green',
    http: 'magenta',
    debug: 'white',
}

winston.addColors(colors)


const format = function (colored = true) {
    return winston.format.combine(
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
        winston.format.colorize({ all: colored }),
        winston.format.printf(
            (info) => `${info.timestamp} ${info.level}: ${info.message}`,
        ),
    )
}
const Logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            format: winston.format.combine(
                winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
                winston.format.json()
            ), filename: 'logs/error.log', level: 'error'
        }),
        new winston.transports.File({ format: format(false), filename: 'logs/combined.log', level: 'debug' }),
        new winston.transports.Console({ format: format(), level: 'debug' }),
    ],
});



export default Logger