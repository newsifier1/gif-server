import desiredConfig from "./giphy_config.js"

export default {
    url: desiredConfig.url,
    api_key: desiredConfig.api_key,
    api_key_query: desiredConfig.api_key_query
}