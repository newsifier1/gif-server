import GifConfig from '../GifConfig/index.js';

export default class GifServiceAPI {
	url = '';

	constructor() {
		// 1- build the base url
		this.url = `${GifConfig.url}?${GifConfig.api_key_query}=${GifConfig.api_key}&`;
	}

	query(key, value) {
		this.url += `${key}=${value}&`;
	}

	build() {
		return this.url;
	}
}
